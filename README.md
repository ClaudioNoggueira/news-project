# news-project

## Documentation

<img src="https://github.com/ClaudioNoggueira/news-project/blob/main/screenshots/swagger-ui-frontpage.png" alt="Swagger UI frontpage" />

<img src="https://github.com/ClaudioNoggueira/news-project/blob/main/screenshots/AuthorController.png" alt="Swagger UI frontpage author-controler dropdown" />

<img src="https://github.com/ClaudioNoggueira/news-project/blob/main/screenshots/CategoryController.png" alt="Swagger UI frontpage category-controler dropdown" />

<img src="https://github.com/ClaudioNoggueira/news-project/blob/main/screenshots/NewsController.png" alt="Swagger UI frontpage news-controler dropdown" />

## Features

### Author

- Add author
- Update author
- Find all authors
- Find author by id
- Find author by email
- Find author by first name
- Find author by last name
- Find author by full name

### Category

- Add category
- Update category
- Find all categories
- Find category by id
- Find category by name

### News

- Add news
- Update news
- Find all news
- Find news by id
- Find news by title
- Find news by author
- Delete news

## Class Diagram

<img src="https://github.com/ClaudioNoggueira/news-project/blob/main/diagrams/class-diagram.png" alt="Class-diagram"/>
